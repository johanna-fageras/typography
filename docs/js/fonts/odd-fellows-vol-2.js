window.addEventListener("load", function () {
  var fonts = [
    {
      "name": "Albro",
      "files": ["/fonts/odd-fellows-vol-2/OddFellow-Albro.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Albro.woff"]
    },
    {
      "name": "Banjo",
      "files": ["/fonts/odd-fellows-vol-2/OddFellow-Banjo.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Banjo.woff"]
    },
    {
      "name": "Circe",
      "files": ["/fonts/odd-fellows-vol-2/OddFellow-Circe.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Circe.woff"]
    },
    {
      "name": "F37X",
      "files": ["/fonts/odd-fellows-vol-2/OddFellow-F37X.woff2", "/fonts/odd-fellows-vol-1/OddFellow-F37X.woff"]
    },
    {
      "name": "Italiano",
      "files": ["/fonts/odd-fellows-vol-2/OddFellow-Italiano.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Italiano.woff"]
    },
    {
      "name": "Neoreby",
      "files": ["/fonts/odd-fellows-vol-2/OddFellow-Neoreby.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Neoreby.woff"]
    },
    {
      "name": "Outline",
      "files": ["/fonts/odd-fellows-vol-2/OddFellow-Outline.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Outline.woff"]
    },
    {
      "name": "Regatta",
      "files": ["/fonts/odd-fellows-vol-2/OddFellow-Regatta.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Regatta.woff"]
    },
    {
      "name": "Sunset",
      "files": ["/fonts/odd-fellows-vol-2/OddFellow-Sunset.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Sunset.woff"]
    },
    {
      "name": "Tilt Warp",
      "files": ["/fonts/odd-fellows-vol-2/OddFellow-TiltWarp.woff2", "/fonts/odd-fellows-vol-1/OddFellow-TiltWarp.woff"]
    }
  ]

  var options = {
    multiline: true,
    lazyload: true,
    order: [
      ["fontsize", "lineheight", "letterspacing"],
      ["fontfamily", "alignment", ],
      "tester"
    ],
    config: {
      tester: {
        editable: true,
        label: false
      },
      fontsize: {
        unit: "rem",
        init: 3.5,
        min: 0.5,
        max: 6.5,
        step: 0.01,
        label: "Size"
      },
      lineheight: {
        unit: "%",
        init: 100,
        min: 60,
        max: 140,
        step: 1,
        label: "Leading"
      },
      letterspacing: {
        unit: "em",
        init: 0,
        min: -1,
        max: 1,
        step: 0.01,
        label: "Letterspacing"
      },
      fontfamily: {
        label: "Style",
        init: "Italiano"
      },
      alignment: {
        choices: ["left|Left", "center|Centered", "right|Right"],
        init: "Center",
        label: "Alignment"
      }
    }
  }
    
  var fs = new Fontsampler(document.getElementById("demo"), fonts, options)
  fs.addEventListener("fontsampler.events.valuechanged", function (e) {
    console.debug("valuechanged event triggered", e.detail)
  })
  FontsamplerSkin(fs)
  fs.init()
});

tinymce.init({
  selector: 'textarea#editor',
  min_height: 500,
  branding: false,
  promotion: false,
  elementpath: false,
  resize: false,
  inline: false,
  ui_mode: 'split',
  skin: 'snow',
  icons: 'bootstrap',
  icon_url: '/js/tinymce/skins/ui/snow/skin.css',
  plugins: 'preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed codesample table charmap pagebreak nonbreaking anchor tableofcontents insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker help formatpainter pageembed charmap mentions quickbars linkchecker emoticons advtable footnotes mergetags autocorrect typography advtemplate',
  toolbar: 'fontfamily fontsize | bold italic underline | forecolor backcolor | alignleft aligncenter alignright',
  menubar: 'file edit insert format view tools table',
  quickbars_insert_toolbar: false,
  quickbars_selection_toolbar: false,
  font_css: '/css/fonts/odd-fellows-vol-2.css',
  font_family_formats: 'Albro=albro; Banjo=banjo; Circe=circe; F37X=f37x; Italiano=italiano; Neoreby=neoreby; Outline=outline; Regatta=regatta; Sunset=sunset; Tilt Warp=tilt warp',
  font_size_input_default_unit: "px",
  font_size_formats: '8px 10px 12px 14px 16px 18px 24px 36px 48px 60px 72px',
  content_style: 'body { font-family:"Italiano",Arial,sans-serif; font-size:14px; }',
  color_map: [
    '#D3F9D8', 'White Green',
    '#FFF3BF', 'White Amber',
    '#FFE3E3', 'White Red',
    '#FFDEEB', 'White Pink',
    '#E5DBFF', 'White Purple',
    '#D0EBFF', 'White Blue',
    '#F1F3F5', 'White Gray',
    
    '#8CE99A', 'Pale Green',
    '#FFE066', 'Pale Amber',
    '#FFA8A8', 'Pale Red',
    '#FAA2C1', 'Pale Pink',
    '#B197FC', 'Pale Purple',
    '#74C0FC', 'Pale Blue',
    '#DEE2E6', 'Pale Gray',
    
    '#51CF66', 'Light Green',
    '#FCC419', 'Light Amber',
    '#FF6B6B', 'Light Red',
    '#F06595', 'Light Pink',
    '#845EF7', 'Light Purple',
    '#339AF0', 'Light Blue',
    '#ADB5BD', 'Light Gray',

    '#37B24D', 'Medium Green',
    '#F59F00', 'Medium Amber',
    '#F03E3E', 'Medium Red',
    '#D6336C', 'Medium Pink',
    '#7048E8', 'Medium Purple',
    '#1C7ED6', 'Medium Blue',
    '#495057', 'Medium Gray',

    '#2B8A3E', 'Dark Green',
    '#E67700', 'Dark Amber',
    '#C92A2A', 'Dark Red',
    '#A61E4D', 'Dark Pink',
    '#5F3DC4', 'Dark Purple',
    '#1864AB', 'Dark Blue',
    '#212529', 'Dark Gray',

    '#000000', 'Black',
    '#ffffff', 'White',
    '#ffffff', 'White'
  ],
})