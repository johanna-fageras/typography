window.addEventListener("load", function () {
  var fonts = [
    {
      "name": "Simple",
      "files": ["/fonts/tomarik/Tomarik-Simple.woff2", "/fonts/tomarik/Tomarik-Simple.woff"]
    },
    {
      "name": "Duo",
      "files": ["/fonts/tomarik/Tomarik-Duo.woff2"]
    },
    {
      "name": "Line",
      "files": ["/fonts/tomarik/Tomarik-Line.woff2", "/fonts/tomarik/Tomarik-Line.woff"]
    },
    {
      "name": "Multi",
      "files": ["/fonts/tomarik/Tomarik-Multi.woff2"]
    }
  ]

  var options = {
    multiline: true,
    lazyload: true,
    order: [
      ["fontsize", "lineheight", "letterspacing"],
      ["fontfamily", "alignment", ],
      "tester"
    ],
    config: {
      tester: {
        editable: true,
        label: false
      },
      fontsize: {
        unit: "rem",
        init: 3.5,
        min: 0.5,
        max: 6.5,
        step: 0.01,
        label: "Size"
      },
      lineheight: {
        unit: "%",
        init: 100,
        min: 60,
        max: 140,
        step: 1,
        label: "Leading"
      },
      letterspacing: {
        unit: "em",
        init: 0,
        min: -1,
        max: 1,
        step: 0.01,
        label: "Letterspacing"
      },
      fontfamily: {
        label: "Style",
        init: "Multi"
      },
      alignment: {
        choices: ["left|Left", "center|Centered", "right|Right"],
        init: "Center",
        label: "Alignment"
      }
    }
  }
    
  var fs = new Fontsampler(document.getElementById("demo"), fonts, options)
  fs.addEventListener("fontsampler.events.valuechanged", function (e) {
    console.debug("valuechanged event triggered", e.detail)
  })
  FontsamplerSkin(fs)
  fs.init()
});

tinymce.init({
  selector: 'textarea#editor',
  min_height: 500,
  branding: false,
  promotion: false,
  elementpath: false,
  resize: false,
  inline: false,
  ui_mode: 'split',
  skin: 'snow',
  icons: 'bootstrap',
  icon_url: '/js/tinymce/skins/ui/snow/skin.css',
  plugins: 'preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed codesample table charmap pagebreak nonbreaking anchor tableofcontents insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker help formatpainter pageembed charmap mentions quickbars linkchecker emoticons advtable footnotes mergetags autocorrect typography advtemplate',
  toolbar: 'fontfamily fontsize | bold italic underline | forecolor backcolor | alignleft aligncenter alignright',
  menubar: 'file edit insert format view tools table',
  quickbars_insert_toolbar: false,
  quickbars_selection_toolbar: false,
  font_css: '/css/fonts/tomarik.css',
  font_family_formats: 'Tomarik Simple=tomarik simple; Tomarik Duo=tomarik duo; Tomarik Line=tomarik line; Tomarik Multi=tomarik multi',
  font_size_input_default_unit: "px",
  font_size_formats: '8px 10px 12px 14px 16px 18px 24px 36px 48px 60px 72px',
  content_style: 'body { font-family:"Tomarik Multi",Arial,sans-serif; font-size:14px; }',
  color_map: [
    '#66d6a4', 'White Green',
    '#ffe280', 'White Amber',
    '#ff7386', 'White Red',
    '#fc7bbf', 'White Pink',
    '#a991f1', 'White Purple',
    '#94c2f4', 'White Blue',
    '#F1F3F5', 'White Gray',
    
    '#33c885', 'Pale Green',
    '#ffdc66', 'Pale Amber',
    '#ff455d', 'Pale Red',
    '#fb4fa9', 'Pale Pink',
    '#8c6ded', 'Pale Purple',
    '#71aef1', 'Pale Blue',
    '#DEE2E6', 'Pale Gray',
    
    '#00ba67', 'Light Green',
    '#ffd64d', 'Light Amber',
    '#ff1635', 'Light Red',
    '#fa2394', 'Light Pink',
    '#6f48e8', 'Light Purple',
    '#4d9aed', 'Light Blue',
    '#ADB5BD', 'Light Gray',

    '#009552', 'Medium Green',
    '#ffd133', 'Medium Amber',
    '#cc122a', 'Medium Red',
    '#c81c76', 'Medium Pink',
    '#593aba', 'Medium Purple',
    '#3e7bbe', 'Medium Blue',
    '#495057', 'Medium Gray',

    '#00703e', 'Dark Green',
    '#ffc500', 'Dark Amber',
    '#990d20', 'Dark Red',
    '#961559', 'Dark Pink',
    '#432b8b', 'Dark Purple',
    '#2e5c8e', 'Dark Blue',
    '#212529', 'Dark Gray',

    '#000000', 'Black',
    '#ffffff', 'White',
    '#ffffff', 'White'
  ],
})
