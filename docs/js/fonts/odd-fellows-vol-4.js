window.addEventListener("load", function () {
  var fonts = [
    {
      "name": "Aparo",
      "files": ["/fonts/odd-fellows-vol-4/OddFellow-Aparo.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Aparo.woff"]
    },
    {
      "name": "Blandit",
      "files": ["/fonts/odd-fellows-vol-4/OddFellow-Blandit.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Blandit.woff"]
    },
    {
      "name": "Estilo",
      "files": ["/fonts/odd-fellows-vol-4/OddFellow-Estilo.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Estilo.woff"]
    },
    {
      "name": "Flourish",
      "files": ["/fonts/odd-fellows-vol-4/OddFellow-Flourish.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Flourish.woff"]
    },
    {
      "name": "Luxae",
      "files": ["/fonts/odd-fellows-vol-4/OddFellow-Luxae.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Luxae.woff"]
    },
    {
      "name": "Norwester",
      "files": ["/fonts/odd-fellows-vol-4/OddFellow-Norwester.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Norwester.woff"]
    },
    {
      "name": "Quasith",
      "files": ["/fonts/odd-fellows-vol-4/OddFellow-Quasith.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Quasith.woff"]
    },
    {
      "name": "Solid",
      "files": ["/fonts/odd-fellows-vol-4/OddFellow-Solid.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Solid.woff"]
    },
    {
      "name": "Sybil",
      "files": ["/fonts/odd-fellows-vol-4/OddFellow-Sybil.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Sybil.woff"]
    },
    {
      "name": "Zart",
      "files": ["/fonts/odd-fellows-vol-4/OddFellow-Zart.woff2", "/fonts/odd-fellows-vol-1/OddFellow-Zart.woff"]
    }
  ]

  var options = {
    multiline: true,
    lazyload: true,
    order: [
      ["fontsize", "lineheight", "letterspacing"],
      ["fontfamily", "alignment", ],
      "tester"
    ],
    config: {
      tester: {
        editable: true,
        label: false
      },
      fontsize: {
        unit: "rem",
        init: 3.5,
        min: 0.5,
        max: 6.5,
        step: 0.01,
        label: "Size"
      },
      lineheight: {
        unit: "%",
        init: 100,
        min: 60,
        max: 140,
        step: 1,
        label: "Leading"
      },
      letterspacing: {
        unit: "em",
        init: 0,
        min: -1,
        max: 1,
        step: 0.01,
        label: "Letterspacing"
      },
      fontfamily: {
        label: "Style",
        init: "Sybil"
      },
      alignment: {
        choices: ["left|Left", "center|Centered", "right|Right"],
        init: "Center",
        label: "Alignment"
      }
    }
  }
    
  var fs = new Fontsampler(document.getElementById("demo"), fonts, options)
  fs.addEventListener("fontsampler.events.valuechanged", function (e) {
    console.debug("valuechanged event triggered", e.detail)
  })
  FontsamplerSkin(fs)
  fs.init()
});

tinymce.init({
  selector: 'textarea#editor',
  min_height: 500,
  branding: false,
  promotion: false,
  elementpath: false,
  resize: false,
  inline: false,
  ui_mode: 'split',
  skin: 'snow',
  icons: 'bootstrap',
  icon_url: '/js/tinymce/skins/ui/snow/skin.css',
  plugins: 'preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed codesample table charmap pagebreak nonbreaking anchor tableofcontents insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker help formatpainter pageembed charmap mentions quickbars linkchecker emoticons advtable footnotes mergetags autocorrect typography advtemplate',
  toolbar: 'fontfamily fontsize | bold italic underline | forecolor backcolor | alignleft aligncenter alignright',
  menubar: 'file edit insert format view tools table',
  quickbars_insert_toolbar: false,
  quickbars_selection_toolbar: false,
  font_css: '/css/fonts/odd-fellows-vol-4.css',
  font_family_formats: 'Aparo=aparo; Blandit=blandit; Estilo=estilo; Flourish=flourish; Luxae=luxae; Norwester=norwester; Quasith=quasith; Solid=solid; Sybil=sybil; Zart=zart',
  font_size_input_default_unit: "px",
  font_size_formats: '8px 10px 12px 14px 16px 18px 24px 36px 48px 60px 72px',
  content_style: 'body { font-family:"Sybil",Arial,sans-serif; font-size:14px; }',
  color_map: [
    '#D3F9D8', 'White Green',
    '#FFF3BF', 'White Amber',
    '#FFE3E3', 'White Red',
    '#FFDEEB', 'White Pink',
    '#E5DBFF', 'White Purple',
    '#D0EBFF', 'White Blue',
    '#F1F3F5', 'White Gray',
    
    '#8CE99A', 'Pale Green',
    '#FFE066', 'Pale Amber',
    '#FFA8A8', 'Pale Red',
    '#FAA2C1', 'Pale Pink',
    '#B197FC', 'Pale Purple',
    '#74C0FC', 'Pale Blue',
    '#DEE2E6', 'Pale Gray',
    
    '#51CF66', 'Light Green',
    '#FCC419', 'Light Amber',
    '#FF6B6B', 'Light Red',
    '#F06595', 'Light Pink',
    '#845EF7', 'Light Purple',
    '#339AF0', 'Light Blue',
    '#ADB5BD', 'Light Gray',

    '#37B24D', 'Medium Green',
    '#F59F00', 'Medium Amber',
    '#F03E3E', 'Medium Red',
    '#D6336C', 'Medium Pink',
    '#7048E8', 'Medium Purple',
    '#1C7ED6', 'Medium Blue',
    '#495057', 'Medium Gray',

    '#2B8A3E', 'Dark Green',
    '#E67700', 'Dark Amber',
    '#C92A2A', 'Dark Red',
    '#A61E4D', 'Dark Pink',
    '#5F3DC4', 'Dark Purple',
    '#1864AB', 'Dark Blue',
    '#212529', 'Dark Gray',

    '#000000', 'Black',
    '#ffffff', 'White',
    '#ffffff', 'White'
  ],
})