module.exports = {
	title: "JF TYPOGRAPHY",
	url: "https://jf-typography.netlify.io",
	language: "en",
	author: {
		name: "Johanna Fagerås",
		email: "heyjohfa@gmail.com",
		url: "https://jf-typography.netlify.io"
	}
}
